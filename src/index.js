import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from "react-router-dom";
import './index.css';

//Pages
import Home from "./pages/home/home";
import ReactGoogleMaps from './pages/reactGoogleMaps/reactGoogleMaps';
import ReactMapGl from './pages/reactMapGl/reactMapGl';
import Recharts from './pages/recharts/recharts';


const routing = (
  <Router>
    <Route exact path='/' component={Home} />
    <Route exact path='/react-google-maps' component={ReactGoogleMaps} />
    <Route exact path='/react-map-gl' component={ReactMapGl} />
    <Route exact path='/recharts' component={Recharts} />
  </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
