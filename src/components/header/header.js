import React from "react";

import "./header.scss";

function AppHeader(props) {
  return (
    <header className="panel-header">
      <h1>{props.title}</h1>
    </header>
  );
}

export default AppHeader;
