import React from "react";

// CSS
import "./sidebar.scss";

function Sidebar() {
  return (
    <nav>
      <a href="/" className="nav-logo">
        <p>Bogdan Mihai</p>
      </a>
      <a href="/">
        <span>Home</span>
      </a>
      <a href="/react-google-maps">
        <span>react-google-maps</span>
      </a>
      <a href="/react-map-gl">
        <span>react-map-gl</span>
      </a>
      <a href="/recharts">
        <span>recharts</span>
      </a>
    </nav>
  );
}

export default Sidebar;
