import React from 'react';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import './chart.scss'
import * as myPoints from "../../data/pois.json";

let counts = [0, 0, 0];
let sports = [0, 0, 0];
let food = [0, 0, 0];

myPoints.features.map(point => {


  if (point.properties.ZONE === 'A' && point.properties.sports === 1) {
    counts[0] += 1;
    sports[0] += 1;
  } else if (point.properties.ZONE === 'A' && point.properties.food === 1) {
    counts[0] += 1;
    food[0] += 1;
  } else if (point.properties.ZONE === 'A') {
    counts[0] += 1;
  }

  if (point.properties.ZONE === 'B' && point.properties.sports === 1) {
    counts[1] += 1;
    sports[1] += 1;
  } else if (point.properties.ZONE === 'B' && point.properties.food === 1) {
    counts[1] += 1;
    food[1] += 1;
  } else if (point.properties.ZONE === 'B') {
    counts[1] += 1;
  }

  if (point.properties.ZONE === 'C' && point.properties.sports === 1) {
    counts[2] += 1;
    sports[2] += 1
  } else if (point.properties.ZONE === 'C' && point.properties.food === 1) {
    counts[2] += 1;
    food[2] += 1;
  } else if (point.properties.ZONE === 'C') {
    counts[2] += 1;
  }

  return counts
});

// const countSports


const data = [
  {
    name: 'Zone A', locations: counts[0], sports: sports[0], food: food[0]
  },
  {
    name: 'Zone B', locations: counts[1], sports: sports[1], food: food[1]
  },
  {
    name: 'Zone C', locations: counts[2], sports: sports[2], food: food[2]
  }
];

class Chart extends React.Component {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/30763kr7/';

  render() {
    return (
      <BarChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="locations" fill="#8884d8" />
        <Bar dataKey="sports" fill="#8bb4d8" />
        <Bar dataKey="food" fill="#8bb444" />
      </BarChart>
    );
  }
}

export default Chart;
