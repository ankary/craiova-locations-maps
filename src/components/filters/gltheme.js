import React from 'react';
import './gltheme.scss'

///// Mapbox Styles
import * as mapStyles from '../../data/mapStyles'

class GlTheme extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultValue: 2
    }
    // Bind methods so can be passed as props to gltheme component
    // this.setTheme = this.setTheme.bind(this);
  }


  render() {
    return (
      <span id='theme' className='theme'>
        <p>Choose map style: </p>
        <select defaultValue={this.state.defaultValue} id="themeoptions" onChange={this.props.setTheme}>
          {mapStyles.style.map(option =>
            <option key={option.id} value={option.id}>{option.name}</option>
          )}
        </select>
      </span >
    )
  }

}

export default GlTheme
