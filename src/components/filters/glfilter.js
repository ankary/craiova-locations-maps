import React from 'react';
import './glfilter.scss'

class GlFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedOption: '1'
    }

    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  handleOptionChange(changeEvent) {
    this.setState({
      selectedOption: changeEvent.target.value
    });
  }



  render() {
    return (
      <span className='filter'>
        <input type="radio" value='1' id="al" name="filter" checked={this.state.selectedOption === '1'} onChange={this.handleOptionChange} />
        <label htmlFor="al" onClick={this.props.aP}>All Locations</label>
        <input type="radio" value='2' id="fl" name="filter" checked={this.state.selectedOption === '2'} onChange={this.handleOptionChange} />
        <label htmlFor="fl" onClick={this.props.fP}>FOOD Locations</label>
        <input type="radio" value='3' id="sl" name="filter" checked={this.state.selectedOption === '3'} onChange={this.handleOptionChange} />
        <label htmlFor="sl" onClick={this.props.sP}>Sports Locations</label>
      </span>
    )
  }
}

export default GlFilter

// mapbox://styles/mapbox/streets-v9
// mapbox://styles/mapbox/dark-v9
// mapbox://styles/mapbox/light-v9
