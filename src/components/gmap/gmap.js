import React, { useState, useEffect, useCallback } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";
import * as myPoints from "../../data/pois.json";
import mapStyles from "../../mapStyles";
import './gmap.scss'

function Map() {
  const [selectedPoint, setSelectedPoint] = useState(null);
  const [displayedPoints, setDisplayedPoints] = useState(myPoints.features);

  const foodPoints = myPoints.features.filter(point => point.properties.food === 1)
  const sportsPoints = myPoints.features.filter(point => point.properties.sports === 1)

  useEffect(() => {
    const listener = e => {
      if (e.key === "Escape") {
        setSelectedPoint(null);
      }
    };
    window.addEventListener("keydown", listener);

    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, []);

  const updateFilter = useCallback(async () => {

    const f = document.getElementById("filterdd");
    const strUser = f.options[f.selectedIndex].text;

    if (strUser === 'food') {
      setDisplayedPoints(foodPoints)
      setSelectedPoint(null)
    } else if (strUser === 'sports') {
      setDisplayedPoints(sportsPoints)
      setSelectedPoint(null);
    } else {
      setDisplayedPoints(myPoints.features)
      setSelectedPoint(null);
    }
  }, [displayedPoints]);

  return (
    <div>
      <div className='filters'>
        <p>Filter map locations: </p>
        <select id="filterdd" onChange={updateFilter}>
          <option>All</option>
          <option>food</option>
          <option>sports</option>
        </select>
      </div>


      <div>
        <GoogleMap
          defaultZoom={14}
          defaultCenter={{ lat: 44.318378, lng: 23.796400 }}
          defaultOptions={{ styles: mapStyles }}
        >
          {displayedPoints.map(point => (
            <Marker
              key={point.properties.POINT_ID}

              position={{
                lat: point.geometry.coordinates[1],
                lng: point.geometry.coordinates[0]
              }}
              onClick={() => {
                setSelectedPoint(point);
              }}
              icon={{
                url: point.properties.ICON,
                scaledSize: new window.google.maps.Size(25, 25)
              }}
            />
          ))}

          {selectedPoint && (
            <InfoWindow
              onCloseClick={() => {
                setSelectedPoint(null);
              }}
              position={{
                lat: selectedPoint.geometry.coordinates[1],
                lng: selectedPoint.geometry.coordinates[0]
              }}
              pixelOffset={1000}
            >
              <div>
                <h2>{selectedPoint.properties.NAME}</h2>
                <p className='description'> {selectedPoint.properties.DESCRIPTIO}</p>
              </div>
            </InfoWindow>
          )
          }
        </GoogleMap >
      </div>
    </div>

  );
}

const MapWrapped = withScriptjs(withGoogleMap(Map));

export default function GMap() {
  return (
    <div className="gmap">
      <MapWrapped
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyAkPBILcLPQotjxeL_qUMtoiGWlROeX7KU`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  );
}
