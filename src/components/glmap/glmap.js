import React from 'react';
import "./glmap.scss"
import { StaticMap } from 'react-map-gl';
import DeckGL from '@deck.gl/react';
import { HeatmapLayer } from '@deck.gl/aggregation-layers';
import GlFilter from '../../components/filters/glfilter';
import GlTheme from '../../components/filters/gltheme';

///// Points of Interest
import * as myPoints from "../../data/pois.json";

///// Mapbox Styles
import * as mapStyles from '../../data/mapStyles'

///// Token for Mapbox
const MAPBOX_TOKEN = 'pk.eyJ1IjoiYW5rYXJ5IiwiYSI6ImNrN2M4bXlsZDA5OHQzbXBkMnY1Zjg3eTQifQ.MC7jZwNtByJ16idvehtulw';

///// GET All points coordinates from ../../data/pois.json
const allPoints = myPoints.features.map(point => {
  return point.geometry.coordinates;
})

///// GET Sport points coordinates from ../../data/pois.json
const sportsPoints = myPoints.features.filter(point => point.properties.sports === 1).map(sp => {
  return sp.geometry.coordinates;
})

///// GET FOOD points coordinates from ../../data/pois.json
const foodPoints = myPoints.features.filter(point => point.properties.food === 1).map(fp => {
  return fp.geometry.coordinates;
})

const INITIAL_VIEW_STATE = {
  longitude: 23.79,
  latitude: 44.31,
  zoom: 12,
  maxZoom: 16,
  pitch: 0,
  bearing: 0
};

class GlMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      points: allPoints,
      theme: 'mapbox://styles/mapbox/dark-v9'
    }
    // Bind methods so can be passed as props to glfilter component
    this.aP = this.aP.bind(this);
    this.fP = this.fP.bind(this);
    this.sP = this.sP.bind(this);
    this.setTheme = this.setTheme.bind(this);
  }

  setTheme(e) {
    e.persist();
    const index = document.getElementById("themeoptions").selectedIndex;

    this.setState({
      theme: mapStyles.style[index].style
    })

    if (index % 2 === 0) {
      document.getElementById('theme').classList.add('blacktext')
    } else {
      document.getElementById('theme').classList.remove('blacktext')
    }
  }

  aP() {
    this.setState({
      points: allPoints
    });
  }

  fP() {
    this.setState({
      points: foodPoints
    });
  }

  sP() {
    this.setState({
      points: sportsPoints
    });
  }

  _renderLayers() {

    const { data = this.state.points, intensity = 1, threshold = 0.03, radiusPixels = 35 } = this.props;

    return [
      new HeatmapLayer({
        data,
        id: 'heatmp-layer',
        pickable: false,
        getPosition: d => [d[0], d[1]],
        // getWeight: d => d[2],
        getWeight: 1,
        radiusPixels,
        intensity,
        threshold
      })
    ];
  }

  render() {
    const { mapStyle = this.state.theme } = this.props;
    const defaultValue = 2;

    return (
      <div className='glmap' >
        <GlTheme setTheme={this.setTheme}></GlTheme>
        <GlFilter aP={this.aP} fP={this.fP} sP={this.sP}></GlFilter>

        <DeckGL
          initialViewState={INITIAL_VIEW_STATE}
          controller={true}
          layers={this._renderLayers()}
        >
          <StaticMap
            reuseMaps
            mapStyle={mapStyle}
            preventStyleDiffing={true}
            mapboxApiAccessToken={MAPBOX_TOKEN}
          />
        </DeckGL>
      </div >
    );
  }
}

export default GlMap;
