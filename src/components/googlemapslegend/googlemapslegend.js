import React from 'react';
import './googlemapslegend.scss'

class GoogleMapsLegend extends React.Component {
  render() {
    return (
      <div className='legend'>
        <div className='legend__items'>
          <div className='legend__section'>
            <div className='legend__item'>
              <img className='legend__icon' src='/home.svg' alt='Home' /> <p>Home</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./office.svg' alt='Office' /> <p>Office</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./football.svg' alt='Sports Club [mainly football]' /> <p>Sports Club [mainly football]</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./tennis.svg' alt='Sports Club [mainly tennis]' /> <p>Sports Club [mainly tennis]</p>
            </div>
          </div>
          <div className='legend__section'>
            <div className='legend__item'>
              <img className='legend__icon' src='./ping-pong.svg' alt='Sports Club [mainly ping-pong]' /> <p>Sports Club [mainly ping-pong]</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./movie.svg' alt='Cinema' /> <p>Cinema</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./gym.svg' alt='Gym' /> <p>Gym</p>
            </div>
            <div className='legend__item'>
              <img className='legend__icon' src='./school.svg' alt='School' /> <p>School</p>
            </div>
          </div>
          <div className='legend__section'>
            <div className='legend__item'>
              <img className='legend__icon' src='./food.svg' alt='FOOD' /> <p>FOOD</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default GoogleMapsLegend
