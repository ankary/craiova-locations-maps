import React from 'react';
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';
import './home.scss';


class Home extends React.Component {
  render() {
    return <div className="container">
      <aside>
        <Sidebar></Sidebar>
      </aside>
      <main>
        <AppHeader title="My Points of Interest"></AppHeader>
        <div className="home">
          <p className='project-description'>Here you can find some locations in Craiova where I live, work and spend my free time.</p>
          <p className='project-description'>Using left side navigation you can see the implementation of each library used in this project.</p>
          <p className='project-description'>Also please see below a short description for each of them:</p>
          <ul>
            <li className='library-description'>react-google-maps: Some locations in Craiova where I live, work and spend my free time displayed on Google Maps using react-google-maps library; by clicking each point an info-window will open showing some details about that place. You can filter location by using the dropdown menu from the top-right corner of the map.</li>
            <li className='library-description'>react-map-gl: The same location points from Craiova are displayed on Mapbox using react-map-gl library; a Heatmap layer is also used. You can filter the location by clicking the desired button  at the top of the page</li>
            <li className='library-description'>recharts: A barchart showing in which zone of the city (zone A, B or C) the locations are using recharts library</li>
          </ul>
        </div>
      </main>
    </div>
  }
}

export default Home;
