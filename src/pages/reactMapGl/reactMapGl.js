import React from 'react'
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';
import GlMap from '../../components/glmap/glmap';
import './reactMapGl.scss';



class ReactMapGl extends React.Component {
  render() {
    return <div className="container">
      <aside>
        <Sidebar></Sidebar>
      </aside>
      <main>
        <div className='title'>
          <AppHeader title="Points of Interest using react-maps-gl"></AppHeader>
          {/* <GlFilter></GlFilter> */}
        </div>
        <GlMap></GlMap>
      </main>
    </div>
  }
}

export default ReactMapGl;
