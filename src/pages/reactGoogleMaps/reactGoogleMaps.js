import React from 'react';
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';
import GMap from '../../components/gmap/gmap';
import GoogleMapsLegend from '../../components/googlemapslegend/googlemapslegend';
import './reactGoogleMaps.scss'



class ReactGoogleMaps extends React.Component {
  render() {
    return <div className="container">
      <aside>
        <Sidebar></Sidebar>
      </aside>
      <main>
        <div className='title'>
          <AppHeader title="Points of Interest using react-google-maps"></AppHeader>
          <GoogleMapsLegend></GoogleMapsLegend>

        </div>
        <GMap></GMap>
      </main>
    </div>
  }
}

export default ReactGoogleMaps;
