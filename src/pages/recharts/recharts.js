import React from 'react'
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';
import Chart from '../../components/chart/chart';


class Recharts extends React.Component {
  render() {
    return <div className="container">
      <aside>
        <Sidebar></Sidebar>
      </aside>
      <main>
        <AppHeader title="Points of Interest using react-maps-gl"></AppHeader>
        <Chart></Chart>
      </main>
    </div>
  }
}

export default Recharts;
