export const style = [
  {
    id: 1,
    style: 'mapbox://styles/mapbox/streets-v11',
    name: 'Streets'
  },
  {
    id: 2,
    style: 'mapbox://styles/mapbox/dark-v10',
    name: 'Dark'
  },
  {
    id: 3,
    style: 'mapbox://styles/mapbox/outdoors-v11',
    name: 'Outdoors'
  },
  {
    id: 4,
    style: 'mapbox://styles/mapbox/satellite-v9',
    name: 'Satellite'
  },
  {
    id: 5,
    style: 'mapbox://styles/mapbox/light-v10',
    name: 'Light'
  },
  {
    id: 6,
    style: 'mapbox://styles/mapbox/satellite-streets-v11',
    name: 'Satellite Streets'
  },
  {
    id: 7,
    style: 'mapbox://styles/mapbox/navigation-preview-day-v4',
    name: 'NavPreview [D]'
  },
  {
    id: 8,
    style: 'mapbox://styles/mapbox/navigation-preview-night-v4',
    name: 'NavPreview [N]'
  },
  {
    id: 9,
    style: 'mapbox://styles/mapbox/navigation-guidance-day-v4',
    name: 'NavGuidance [D]'
  },
  {
    id: 10,
    style: 'mapbox://styles/mapbox/navigation-guidance-night-v4',
    name: 'NavGuidance [N]'
  }
]
